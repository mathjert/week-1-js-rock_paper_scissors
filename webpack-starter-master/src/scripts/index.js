import '../styles/index.scss';
const buttons = document.getElementsByTagName('button');

for(let i = 0; i < buttons.length; i++){
    buttons[i].addEventListener('click', function(){
        playerChoice(i);
    });
}

const npcChoice = () => {
    return Math.floor(Math.random() * 3);
};

function playerChoice(inVar){
    console.log(`Players choice ${inVar}`);
    playGame(inVar);
}

function playGame(playerChoiceVar){
    let npcChoiceVar = npcChoice();
    let winner = 'loser';
    console.log(`NPC choice ${npcChoiceVar}`);
    if(npcChoiceVar === playerChoiceVar){
        winner = 'tie';
    }else {
        switch(playerChoiceVar){
            case 0:
                if(npcChoiceVar === 2){
                    console.log('player wins with rock');
                    winner = 'winner';
                }else {
                    console.log('npc wins');
                }
                break;
            case 1:
                if(npcChoiceVar === 0){
                    console.log('player wins');
                    winner = 'winner';
                }else{
                    console.log('npc wins ');
                }
                break;
            case 2:
                if(npcChoiceVar === 1){
                    console.log('player wins');
                    winner = 'winner';
                }else {
                    console.log('npc wins');
                }
                break;
            default:
                console.log('Error!');
                break;
        }
    }
   
    if(winner === 'winner'){
        //http://cdn.shopify.com/s/files/1/0016/0425/5788/products/SD1798_600x.jpg?v=1543462021
        document.getElementById('output').innerHTML = `<img src="http://cdn.shopify.com/s/files/1/0016/0425/5788/products/SD1798_600x.jpg?v=1543462021" />`;
    }else if(winner === 'tie'){
        document.getElementById('output').innerHTML = `<img src="https://i.ytimg.com/vi/WWfPVqPEoj8/maxresdefault.jpg" width="50%" height="50%" />`;
    }else {
        //https://i0.wp.com/www.losersociety.com/wp-content/uploads/2017/09/loser.png?resize=300%2C250
        document.getElementById('output').innerHTML = `<img src="https://i0.wp.com/www.losersociety.com/wp-content/uploads/2017/09/loser.png?resize=300%2C250" />`;
    }
}
